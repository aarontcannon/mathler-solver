# Description
Solver for the mathler.com game

# Setup
Create virtual environment:  
`python -m venv ./.venv`  
Activate (windows-powershell):  
`.\.venv\Scripts\activate.ps1`  
Activate (linux):  
`source ./.venv/bin/activate`  
Install dependencies:  
`pip install -r ./requirements.txt`  

# Usage
```
usage: mathler.py [-h] [-v] [-l LENGTH] [-m MAX_GUESSES] [-g] [-i INPUT] [-o OUTPUT] total

Solver for the mathler.com game

positional arguments:
  total                 total that the calculation solution must equal

options:
  -h, --help            show this help message and exit
  -v, --verbose         whether to print debug/computation information
  -l LENGTH, --length LENGTH
                        length of the hidden calculation solution, defaults to 6
  -m MAX_GUESSES, --max-guesses MAX_GUESSES
                        maximum number of guesses allowed, defaults to 6
  -g, --generate        whether to (re)generate all possible solutions for a given length
  -i INPUT, --input INPUT
                        file to load solution data from
  -o OUTPUT, --output OUTPUT
                        file to load solution data to
```

# Lessons Learned
- Information Theory
    - [3Blue1Brown Video](https://www.youtube.com/watch?v=v68zYyaEmEA&t=227s)
    - Information and bits
    - Entropy
- Creating my own HashBucket for storing multiple values under the same key/hash
- Sorting dictionaries and using python's sorted function
- Found py_expressions library for parsing/computing mathematical equations stored as strings
- Learned how to convert between lists of items and dictionaries to accomplish different operations
- Learned to store computational results in files when those results won't change and will be needed each time program is run
- Used recursion to generate permutations of solutions and results
    -  They are on the order of n^x which is why recursion was required
- Used argparse library to take in user input when program is called
- Learned to come at problem from different angle to optimize performance without sacrificing clarity
    - In this case changing the algorith from result permutaion based to solution based improved performance over a 1000 fold
- Learned more about how mathler works and the different rules it is governed by