import argparse
import csv
import math
import os
import re
from functools import reduce
from py_expression.core import Exp, ExpressionError

def main():
    parser = argparse.ArgumentParser(description='Solver for the mathler.com game')
    parser.add_argument('total', type=int, help='total that the calculation solution must equal')
    parser.add_argument('-v', '--verbose', action='store_true', help='whether to print debug/computation information')
    parser.add_argument('-l', '--length', type=int, default=6, help='length of the hidden calculation solution, defaults to 6')
    parser.add_argument('-m', '--max-guesses', type=int, default=6, help='maximum number of guesses allowed, defaults to 6')
    parser.add_argument('-g', '--generate', action='store_true', help='whether to (re)generate all possible solutions for a given length')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'), help='file to load solution data from')
    parser.add_argument('-o', '--output', type=str, help='file to load solution data to')

    args = parser.parse_args()

    math_solver = MathlerSolver(solution_length=args.length, max_guesses=args.max_guesses, verbose=args.verbose)

    if args.generate:
        math_solver.generate_solutions_map()
    elif args.input:
        math_solver.load_solutions_map_from_file(args.input)
    else:
        math_solver.generate_solutions_map()
    if args.output:
        math_solver.save_solutions_map_to_file(args.output)

    math_solver.play_game(args.total)

class MathlerSolver:
    def __init__(self, solution_length = 6, max_guesses = 6, verbose = False):
        self.DIGITS = '0123456789'
        self.OPERATORS = '+-/*'
        self.CHARACTERS = self.DIGITS + self.OPERATORS
        self.RESULT_OPTIONS = '012'
        self.GUESS_DISPLAY_COUNT = 10
        self.SOLUTION_LENGTH = solution_length
        self.MAX_GUESSES = max_guesses
        self.verbose = verbose
        self.solutions_map = {}

    def generate_solutions_map(self):
        solutions = self.__generate_all_solutions(self.SOLUTION_LENGTH)
        print(f'Generated {len(solutions)} solutions of length {self.SOLUTION_LENGTH}')

        filtered_solutions = self.__filter_valid_solution_strings(solutions)
        print(f'Filtered out {len(solutions)-len(filtered_solutions)} invalid solutions, leaving {len(filtered_solutions)} potentially valid solutions')

        solutions_map = HashBucket(items=filtered_solutions, key_value=self.__compute_solution_total)
        filtered_solutions_map = self.__filter_valid_solution_totals(solutions_map)
        print(f'Found {filtered_solutions_map.bucket_count()} valid solutions for {len(filtered_solutions_map)} different totals')

        self.solutions_map = filtered_solutions_map

    def save_solutions_map_to_file(self, output_filename):
        rows_written = 0
        with open(output_filename, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            for total in self.solutions_map:
                for solution in self.solutions_map[total]:
                    csv_writer.writerow([total, solution])
                    rows_written += 1
        print(f'Wrote {rows_written} rows to file {csv_file.name}')

    def load_solutions_map_from_file(self, input_file):
        csv_reader = csv.reader(input_file)

        solutions_map = HashBucket(items=csv_reader, key_value=lambda row : (str(row[0]), row[1]))
        print(f'Read {solutions_map.bucket_count()} valid solutions for {len(solutions_map)} different totals from file {input_file.name}')

        first_solution = list(solutions_map.values())[0][0]
        if len(first_solution) != self.SOLUTION_LENGTH:
            self.SOLUTION_LENGTH = len(first_solution)
        self.solutions_map = solutions_map

    def play_game(self, total):
        total = str(total)
        possible_solutions = self.solutions_map[total]

        for guess_count in range(1, self.MAX_GUESSES + 1):
            print(f'Guess # {guess_count}')
            print(f'Found {len(possible_solutions)} solutions equalling total {total}')

            optimal_guesses_map = self.__compute_optimal_guesses(possible_solutions)
            print(f'Best {self.GUESS_DISPLAY_COUNT} solutions to guess:')
            keys_to_display = list(optimal_guesses_map.keys())[:self.GUESS_DISPLAY_COUNT]
            best_guesses_to_display = {k: optimal_guesses_map[k] for k in keys_to_display}
            print(best_guesses_to_display)

            guess, result = self.__get_guess_result_from_user(possible_solutions)
            if result == '2' * self.SOLUTION_LENGTH:
                print(f'Congratulations! You solved today\'s problem in {guess_count} guesses')
                break
            elif guess_count == self.MAX_GUESSES:
                print(f'Game over! You failed to solve today\'s problem in {guess_count} guesses')
                break

            possible_solutions = self.__filter_possible_solutions(possible_solutions, guess, result)

    def __generate_all_solutions(self, depth, solution = ''):
        solutions = []
        if depth > 0:
            for character in self.CHARACTERS:
                solutions += self.__generate_all_solutions(depth - 1, solution + character)
        else:
            solutions.append(solution)
        return solutions

    def __filter_valid_solution_strings(self, solutions):
        # Match 1+ 0's followed by a digit or 2+ consecutive operators or a starting operator or an ending operator
        regex_string = f'0+[{self.DIGITS}]|[{self.OPERATORS}]{{2,}}|^[{self.OPERATORS}]|[{self.OPERATORS}]$'
        invalid_pattern = re.compile(regex_string)
        filtered_solutions = list(filter(lambda solution : invalid_pattern.search(solution) == None, solutions))
        return filtered_solutions

    def __filter_valid_solution_totals(self, solutions_map):
        filtered_solutions_dict = filter(lambda item : str(item[0]).isnumeric(), solutions_map.items())
        filtered_solutions_map = HashBucket(filtered_solutions_dict)
        return filtered_solutions_map

    def __compute_solution_total(self, solution):
        exp = Exp()
        try:
            total = exp.parse(solution).eval()
            return str(total), solution
        except (ExpressionError, AttributeError, ZeroDivisionError):
            return None, solution

    def __compute_optimal_guesses(self, solutions):
        solutions_count = len(solutions)
        optimal_guess_map = {}
        optimal_guess_result_map = {}
        for solution in solutions:
            result_map = HashBucket()
            for guess in solutions:
                result = self.__compute_result(solution, guess)
                result_map.bucket_insert(result, guess)
            sorted_result_map = HashBucket(sorted(result_map.items(), key=lambda item : len(item[1]), reverse=True))
            optimal_guess_result_map[solution] = sorted_result_map

            def entropy_aggr(entropy, guesses):
                probability = len(guesses)/solutions_count
                information = math.log2(1/probability)
                return entropy + probability * information
            entropy = reduce(entropy_aggr, sorted_result_map.values(), 0)
            optimal_guess_map[solution] = entropy
        sorted_optimal_guess_map = dict(sorted(optimal_guess_map.items(), key=lambda item : item[1], reverse=True))
        if self.verbose:
            self.__print_guess_result_map(sorted_optimal_guess_map, optimal_guess_result_map)
        return sorted_optimal_guess_map

    def __compute_result(self, solution, guess):
        result = ''
        for index in range(self.SOLUTION_LENGTH):
            guess_character = guess[index]
            if guess_character == solution[index]:
                result += '2'
            elif guess[:index+1].count(guess_character) <= solution.count(guess_character):
                result += '1'
            else:
                result += '0'
        return result

    def __print_guess_result_map(self, guess_map, guess_result_map):
        for solution in guess_map.keys():
            result_map = guess_result_map[solution]
            entropy = guess_map[solution]
            print(solution, result_map.bucket_count(), len(result_map), entropy)
            for result in result_map.keys():
                print('\t', result, len(result_map[result]), str(result_map[result]))

    def __get_guess_result_from_user(self, possible_solutions):
        is_valid_guess = False
        is_valid_result = False
        while not is_valid_guess:
            guess = input(f'Enter guess: ')
            if not guess in possible_solutions:
                print(f'Invalid guess!')
            else:
                is_valid_guess = True
        while not is_valid_result:
            result = input(f'Enter result of guess: ')
            if re.match(f'[{{{self.RESULT_OPTIONS}}}]{{{self.SOLUTION_LENGTH}}}', result) == None:
                print(f'Invalid result!')
            else:
                is_valid_result = True
        return (guess, result)

    def __filter_possible_solutions(self, solutions, guess, result):
        return list(filter(lambda solution : self.__is_possible_solution(solution, guess, result), solutions))

    def __is_possible_solution(self, solution, guess, result):
        guess_result_map= self.__generate_guess_result_map(guess, result)
        solution_list = list(solution)
        for character in self.CHARACTERS:
            character_results = guess_result_map[character]
            for index in range(self.SOLUTION_LENGTH):
                location_result = character_results[index]
                if location_result == '0' and solution_list[index] == character:
                    return False
                elif location_result == '1' and solution_list[index] == character:
                    return False
                elif location_result == '2' and solution_list[index] != character:
                    return False
            if '0' in character_results and solution_list.count(character) > character_results.count('1') + character_results.count('2'):
                return False
            elif solution_list.count(character) < character_results.count('1') + character_results.count('2'):
                return False
        return True

    def __generate_guess_result_map(self, guess, result):
        guess_result_map = {}
        for character in self.CHARACTERS:
            guess_result_map[character] = [' '] * self.SOLUTION_LENGTH
        for index in range(self.SOLUTION_LENGTH):
            guess_result_map[guess[index]][index] = result[index]
        return guess_result_map

# A dictionary of lists allowing multiple values to be inserted under the same key
class HashBucket(dict):
    def __init__(self, dict = None, items = None, key_value = None):
        # Takes an object to be handled by dict parent constructor
        if (dict != None):
            super().__init__(dict)
        # Takes a list of items and a function to generate a key, value pair from each item
        elif (items != None and callable(key_value)):
            super().__init__()
            for item in items:
                key, value = key_value(item)
                self.bucket_insert(key, value)
        else:
            super().__init__()

    # Inserts a value into a list at a given key
    def bucket_insert(self, key, value):
        if(key in self):
            self[key].append(value)
        else:
            self[key] = [value,]

    def bucket_count(self):
        count = 0
        for _, bucket in self.items():
            count += len(bucket)
        return count

if __name__ == "__main__":
    main()
